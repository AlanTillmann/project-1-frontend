import axios from "axios";
import { useEffect, useRef, useState } from "react";
import { useHistory } from "react-router";
import DisplayChat from "./display-chat";

export default function Chat()
{
    const history = useHistory<any>();
    const [messages, setMessages] = useState([]);
    const [users, setUsers] = useState([]);
    const [times, setTimes] = useState(0);
    const emailInput = useRef<any>(null);
    const messageInput = useRef<any>(null);

    async function getMessages()
    {
        if(history.location.state["recipient"])
        {
            const messageArr:any = [];
            let response = await axios.get(`https://messaging-service-dot-wedding-planner-tillmann.ue.r.appspot.com/messages?sender=${history.location.state["email"]}&recipient=${history.location.state["recipient"]}`);
            messageArr.push(response.data);
            response = await axios.get(`https://messaging-service-dot-wedding-planner-tillmann.ue.r.appspot.com/messages?recipient=${history.location.state["email"]}&sender=${history.location.state["recipient"]}`);
            messageArr.push(response.data);
            setMessages(messageArr);
        }
    }

    async function getUsers()
    {
        let response = await axios.get("https://authorization-service-dot-wedding-planner-tillmann.ue.r.appspot.com/users"); // get email accounts.
        let resArr = response.data;
        resArr = resArr.filter((e:any) =>
        {
            if(e !== history.location.state["email"])
            {
                return e;
            }
        });
        setUsers(resArr);
    }

    async function sendMessage()
    {
        const message =
        {
            sender: history.location.state["email"],
            recipient: history.location.state["recipient"],
            message: messageInput.current.value
        };

        // validate emails
        const sender = await axios.get(`https://authorization-service-dot-wedding-planner-tillmann.ue.r.appspot.com/users/${history.location.state["email"]}/verify`);
        const recipient = await axios.get(`https://authorization-service-dot-wedding-planner-tillmann.ue.r.appspot.com/users/${history.location.state["recipient"]}/verify`);

        // send the message
        if(sender && recipient)
        {
            await axios.post("https://messaging-service-dot-wedding-planner-tillmann.ue.r.appspot.com/messages", message);
            messageInput.current.value = "";
        }
        // we have to call this function twice for consistency for some strange reason...
        // getMessages();
        // getMessages();
    }

    if(times === 0)
    {
        getUsers();
        setTimes(1);
    }

    useEffect(() =>
    {
        const timer = setInterval(() => {
            getMessages();
        }, 1000);
        return () => clearTimeout(timer);
    }, []);
    
    return(
        <>
            <h1>Chat</h1>
            <h3>Welcome, {history.location.state["email"]}</h3>

            <button onClick = {()=>history.push("/login")}>Logout</button>

            <button onClick = {()=>
                {
                    delete history.location.state["recipient"];
                    history.push({pathname: "/planner", state: history.location.state})
                }}>Return to planner</button>

            <select ref = {emailInput} onChange = {() =>
                {
                    history.location.state["recipient"] = emailInput.current.value;
                    getMessages();
                }
            }>

            <option>select user</option>
            {
                users.map((u:any) =>
                        <option>{u}</option>
                    )
            }
            </select> 
            <DisplayChat messages={messages} users={users}></DisplayChat>
            <textarea ref={messageInput}></textarea>
            <button onClick={sendMessage}>Send</button>
        </>
    );
}