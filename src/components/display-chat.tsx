
export default function DisplayChat(props:any)
{
    const messages = props.messages;
    
    if(messages[0])
    {
        // get both lists from messages into one list.
        let newList:any = messages[0].concat(messages[1]);
        newList = newList.sort((a:any, b:any) => a.m_id - b.m_id);

        return(
            <>
                {
                    newList.map((m:any) =>
                        <div key = {m.m_id}>
                            <h5>{m.sender} - {m.date} @ {m.time}</h5>
                            <p>{m.message}</p>
                        </div>
                    )
                }
            </>
        );
    }
    else
    {
        return(<></>);
    }
}

